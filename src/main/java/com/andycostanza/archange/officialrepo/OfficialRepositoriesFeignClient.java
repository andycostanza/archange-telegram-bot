package com.andycostanza.archange.officialrepo;

import com.andycostanza.archange.CustomFeignConfiguration;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.Serializable;
import java.util.Optional;

@FeignClient(name="official-repositories-service", url = "https://www.archlinux.org/packages/search/json/", configuration = CustomFeignConfiguration.class)
public interface OfficialRepositoriesFeignClient extends Serializable {
    @RequestMapping(method = RequestMethod.GET, value = "/")
    Optional<OfficialRepositoriesResponseDTO> search(@RequestParam("q") String q);
    @RequestMapping(method = RequestMethod.GET, value = "/")
    Optional<OfficialRepositoriesResponseDTO> info(@RequestParam("name") String name);
}

