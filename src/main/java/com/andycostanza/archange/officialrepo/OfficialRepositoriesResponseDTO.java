package com.andycostanza.archange.officialrepo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
public class OfficialRepositoriesResponseDTO implements Serializable {
    private Integer version;
    private Integer limit;
    private boolean valid;
    @JsonProperty("num_pages")
    private Integer numPages;
    private Integer page;
    @JsonProperty("results")
    private List<OfficialRepositoriesResultDTO> results;
}
