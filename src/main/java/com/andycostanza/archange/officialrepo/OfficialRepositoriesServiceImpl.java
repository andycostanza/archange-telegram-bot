package com.andycostanza.archange.officialrepo;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.telegram.telegrambots.meta.api.methods.ParseMode;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.inlinequery.inputmessagecontent.InputTextMessageContent;
import org.telegram.telegrambots.meta.api.objects.inlinequery.result.InlineQueryResult;
import org.telegram.telegrambots.meta.api.objects.inlinequery.result.InlineQueryResultArticle;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Service
@RequiredArgsConstructor
public class OfficialRepositoriesServiceImpl implements OfficialRepositoriesService {
    private final OfficialRepositoriesFeignClient feignClient;

    @Override
    public List<SendMessage> info(Long chatId, String packageName) {
        var response = feignClient.info(packageName);
        List<SendMessage> messages = new ArrayList<>();
        if (response.isPresent() && !CollectionUtils.isEmpty(response.get()
                                                                       .getResults())) {
            messages = response.get()
                    .getResults()
                    .stream()
                    .map(result -> {
                        var sendMessage = SendMessage.builder()
                                .chatId(Long.toString(chatId))
                                .text(createInfoMessage(result))
                                .build();
                        sendMessage.setParseMode(ParseMode.HTML);
                        return sendMessage;
                    })
                    .collect(Collectors.toList());

        }else {
            messages.add(SendMessage.builder()
                                 .chatId(Long.toString(chatId))
                                 .text("Nothing found in official repository!")
                                 .build());
        }
        return messages;
    }

    @Override
    public List<SendMessage> search(Long chatId, String packageName) {
        var response = feignClient.search(packageName);
        List<SendMessage> messages = new ArrayList<>();
        if (response.isPresent() && !CollectionUtils.isEmpty(response.get()
                                                                       .getResults())) {

            var responseList = response.get()
                    .getResults();
            var partition = IntStream.range(0, responseList.size())
                    .filter(i -> i % 20 == 0)
                    .mapToObj(i -> responseList.subList(i, Math.min(i + 20, responseList.size())))
                    .toList();
            for (var list :
                    partition) {
                var message = list.stream()
                        .map(result -> "- " + createExcerptMessage(result) + "\n")
                        .collect(Collectors.joining());
                var sendMessage = SendMessage.builder()
                        .chatId(Long.toString(chatId))
                        .text(message)
                        .build();
                sendMessage.setParseMode(ParseMode.HTML);
                messages.add(sendMessage);
            }

        } else {
            messages.add(SendMessage.builder()
                                 .chatId(Long.toString(chatId))
                                 .text("Nothing found in official repositories!")
                                 .build());
        }
        return messages;

    }

    private String createInfoMessage(OfficialRepositoriesResultDTO result) {
        var message = new StringBuilder();
        message
                .append("Repository : <code>")
                .append(result.getRepo())
                .append("</code>\n")
                .append("Package name : <code>")
                .append(result.getPkgname())
                .append("</code>\n")
                .append("Version : <code>")
                .append(result.getPkgver())
                .append("</code>\n")
                .append("Description : <code>")
                .append(result.getPkgdesc())
                .append("</code>\n")
                .append("Architecture : <code>")
                .append(result.getArch())
                .append("</code>\n")
                .append("URL : ")
                .append(result.getUrl())
                .append("\n")
                .append("Licenses : <code>")
                .append(result.getLicenses())
                .append("</code>\n")
                .append("Groups : <code>")
                .append(result.getGroups())
                .append("</code>\n")
                .append("Provides : <code>")
                .append(result.getProvides())
                .append("</code>\n")
                .append("Depends : <code>")
                .append(result.getDepends())
                .append("</code>\n")
                .append("Opt. depends : <code>")
                .append(result.getOptdepends())
                .append("</code>\n")
                .append("Conflicts : <code>")
                .append(result.getConflicts())
                .append("</code>\n")
                .append("Replaces : <code>")
                .append(result.getReplaces())
                .append("</code>\n")
                .append("Compressed size : <code>")
                .append(BigDecimal.valueOf(result.getCompressedSize())
                                .divide(BigDecimal.valueOf(1024D * 1024D),
                                        RoundingMode.UP))
                .append(" MiB</code>\n")
                .append("Installed size : <code>")
                .append(BigDecimal.valueOf(result.getInstalledSize())
                                .divide(BigDecimal.valueOf(1024D * 1024D),
                                        RoundingMode.UP))
                .append(" MiB</code>\n")
                .append("Packager : <code>")
                .append(result.getPackager())
                .append("</code>\n")
                .append("Build date : <code>")
                .append(result.getBuildDate())
                .append("</code>\n")
                .append("Flagged : <code>")
                .append(result.getFlagDate() != null ? "Yes" : "No")
                .append("</code>\n");

        return message.toString();
    }

    private String createExcerptMessage(OfficialRepositoriesResultDTO result) {
        var message = new StringBuilder();
        message.append("<strong>")
                .append(result.getRepo())
                .append("/")
                .append(result.getPkgname())
                .append("</strong> <code>")
                .append(result.getPkgver())
                .append("</code> (")
                .append(BigDecimal.valueOf(result.getCompressedSize())
                                .divide(BigDecimal.valueOf(1024D * 1024D),
                                        RoundingMode.UP))
                .append("MiB ")
                .append(BigDecimal.valueOf(result.getInstalledSize())
                                .divide(BigDecimal.valueOf(1024D * 1024D),
                                        RoundingMode.UP))
                .append("Mib) ");
        if (result.getFlagDate() != null) {
            message.append("<strong>(Flagged : ")
                    .append(result.getFlagDate())
                    .append(")</strong>");
        }
        message.append("\n\t<em>")
                .append(result.getPkgdesc())
                .append("</em>");

        return message.toString();
    }

    private String createTitle(OfficialRepositoriesResultDTO result) {
        var message = new StringBuilder();
        message.append(result.getRepo())
                .append("/")
                .append(result.getPkgname())
                .append(" ")
                .append(result.getPkgver());
        if (result.getFlagDate() != null) {
            message.append(" (Flagged)");
        }
        return message.toString();
    }

    @Override
    public List<InlineQueryResult> inlineQuerySearch(String query) {
        var response = feignClient.info(query);
        List<InlineQueryResult> results = new ArrayList<>();
        if (response.isPresent() && !CollectionUtils.isEmpty(response.get()
                                                                       .getResults())) {
            results = response.get()
                    .getResults()
                    .stream()
                    .map(officialRepositoriesResultDTO -> createInlineQueryResultArticle(createTitle(officialRepositoriesResultDTO),
                                                                                         createDescription(officialRepositoriesResultDTO),
                                                                                         createInfoMessage(officialRepositoriesResultDTO)))
                    .collect(Collectors.toList());

        }
        return results;
    }

    private String createDescription(OfficialRepositoriesResultDTO result) {
        return result.getPkgdesc();
    }


    private InlineQueryResultArticle createInlineQueryResultArticle(String title, String description, String messageText) {
        var resultArticle = new InlineQueryResultArticle();
        resultArticle.setId(UUID.randomUUID()
                                    .toString());
        resultArticle.setTitle(title);
        resultArticle.setDescription(description);
        var inputMessageContent = new InputTextMessageContent();
        inputMessageContent.setMessageText(messageText);
        inputMessageContent.setParseMode(ParseMode.HTML);
        resultArticle.setInputMessageContent(inputMessageContent);
        return resultArticle;
    }
}
