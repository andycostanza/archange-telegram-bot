package com.andycostanza.archange.officialrepo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
public class OfficialRepositoriesResultDTO implements Serializable {
    private String pkgname;
    private String pkgbase;
    private String repo;
    private String arch;
    private String pkgver;
    private String pkgrel;
    private Long epoch;
    private String pkgdesc;
    private String url;
    private String filename;
    @JsonProperty("compressed_size")
    private Long compressedSize;
    @JsonProperty("installed_size")
    private Long installedSize;
    @JsonProperty("build_date")
    private LocalDateTime buildDate;
    @JsonProperty("last_update")
    private LocalDateTime lastUpdate;
    @JsonProperty("flag_date")
    private LocalDateTime flagDate;
    private List<String> maintainers;
    private String packager;
    private List<String> groups;
    private List<String> conflicts;
    private List<String> provides;
    private List<String> replaces;
    private List<String> depends;
    private List<String> optdepends;
    private List<String> makedepends;
    private List<String> checkdepends;
    private List<String> licenses;
}
