package com.andycostanza.archange;

import com.andycostanza.archange.aur.AurService;
import com.andycostanza.archange.manpage.ManPageService;
import com.andycostanza.archange.officialrepo.OfficialRepositoriesService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.AnswerInlineQuery;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Component
@Slf4j
@RequiredArgsConstructor
public class ArchangeTelegramBot extends TelegramLongPollingBot {

    public static final String SEARCH_BOT_COMMAND = "/search";
    public static final String INFO_BOT_COMMAND = "/info";
    public static final String AUR_BOT_COMMAND = "/aur";
    public static final String YAY_BOT_COMMAND = "/yay";
    public static final String HELP_BOT_COMMAND = "/help";
    public static final String START_BOT_COMMAND = "/start";
    public static final String MAN_BOT_COMMAND = "/man";
    public static final String BOT_COMMAND_TYPE = "bot_command";

    private final TelegramBotCredentialsProperties credentials;
    private final AurService aurService;
    private final OfficialRepositoriesService officialRepositoriesService;
    private final ManPageService manPageService;

    @Override
    public String getBotToken() {
        return credentials.getToken();
    }

    @Override
    public void onUpdateReceived(Update update) {

        if (isInlineQueryWithAQuery(update)) {
            onInlineQuery(update);
        } else if (isBotCommandMessage(update)) {
            onBotCommandMessage(update);
        }

    }

    private void onBotCommandMessage(Update update) {
        List<SendMessage> messages = new ArrayList<>();
        var chatId = update.getMessage()
                .getChatId();
        var packageNameToSearch = update.getMessage()
                .getText()
                .substring(update.getMessage()
                        .getEntities()
                        .get(0)
                        .getLength()
                )
                .trim();
        log.info("request : {} {}", update.getMessage()
                .getEntities()
                .get(0)
                .getText(), packageNameToSearch);
        if (StringUtils.isBlank(packageNameToSearch)) {
            if (isHelpBotCommand(update)) {
                var helpMessage = """
                        /search packageName -> Search packages in official Arch repositories
                        /aur packageName -> Search packages in AUR
                        /yay packageName -> Search packages in AUR and official Arch repositories like yay AUR Helper
                        /man keyword -> Display de man page of the keyword you search
                        /help - Display this help
                        """;
                messages.add(SendMessage.builder()
                        .chatId(Long.toString(chatId))
                        .text(helpMessage)
                        .build());
            } else if (isStartBotCommand(update)) {
                messages.add(SendMessage.builder()
                        .chatId(Long.toString(chatId))
                        .text("Hi, my name is Archange. I can help you to search package in AUR and/or official Archlinux repositories.\nTo play with me, please use one of slash command.")
                        .build());
            } else {
                messages.add(SendMessage.builder()
                        .chatId(Long.toString(chatId))
                        .text("You need to provide a package name to search")
                        .build());
            }
        } else {
            if (isSearchBotCommand(update)) {
                var packagesFound = officialRepositoriesService.search(chatId,
                        packageNameToSearch);
                messages.addAll(packagesFound);
            } else if (isAurBotCommand(update)) {
                var aurPackagesFound = aurService.search(chatId,
                        packageNameToSearch);
                messages.addAll(aurPackagesFound);
            } else if (isYayBotCommand(update)) {
                var aurPackagesFound = aurService.search(chatId,
                        packageNameToSearch);
                var packagesFound = officialRepositoriesService.search(chatId,
                        packageNameToSearch);
                messages.addAll(packagesFound);
                messages.addAll(aurPackagesFound);
            } else if (isInfoBotCommande(update)) {
                var aurInfoPackagesFound = aurService.info(chatId,
                        packageNameToSearch);
                var infoPackagesFound = officialRepositoriesService.info(chatId,
                        packageNameToSearch);
                messages.addAll(infoPackagesFound);
                messages.addAll(aurInfoPackagesFound);
            } else if (isManBotCommand(update)) {
                var manPageFound = manPageService.search(chatId,
                        packageNameToSearch);
                messages.addAll(manPageFound);
            } else {
                messages.add(SendMessage.builder()
                        .chatId(Long.toString(chatId))
                        .text("This command doesn't not allowed, please use the bot Archange commands to search Arch packages")
                        .build());
            }
        }
        try {
            for (var sendMessage :
                    messages) {
                this.execute(sendMessage);
            }
        } catch (TelegramApiException ex) {
            log.error(ex.getMessage(),
                    ex);
        }
    }

    private boolean isInfoBotCommande(Update update) {
        return isBotCommandStartsWith(update,
                INFO_BOT_COMMAND);
    }


    private boolean isInlineQueryWithAQuery(Update update) {
        return update.hasInlineQuery()
                && StringUtils.isNotBlank(update.getInlineQuery()
                .getQuery());
    }

    private void onInlineQuery(Update update) {
        try {

            var answer = new AnswerInlineQuery();
            answer.setInlineQueryId(UUID.randomUUID()
                    .toString());
            answer.setInlineQueryId(update.getInlineQuery()
                    .getId());
            var inlineQueryResults =
                    officialRepositoriesService.inlineQuerySearch(update.getInlineQuery()
                            .getQuery());
            var aurInlineQueryResult = aurService.inlineQuerySearch(update.getInlineQuery()
                    .getQuery());
            if (!CollectionUtils.isEmpty(aurInlineQueryResult)) {
                inlineQueryResults.addAll(aurInlineQueryResult);
            }

            answer.setResults(inlineQueryResults.subList(0,
                    Math.min(inlineQueryResults.size(),
                            50)));
            this.execute(answer);

        } catch (TelegramApiException ex) {
            log.error(ex.getMessage(),
                    ex);
        }
    }

    private boolean isStartBotCommand(Update update) {
        return isBotCommandStartsWith(update,
                START_BOT_COMMAND);
    }

    private boolean isHelpBotCommand(Update update) {
        return isBotCommandStartsWith(update,
                HELP_BOT_COMMAND);
    }

    private boolean isYayBotCommand(Update update) {
        return isBotCommandStartsWith(update,
                YAY_BOT_COMMAND);
    }

    private boolean isAurBotCommand(Update update) {
        return isBotCommandStartsWith(update,
                AUR_BOT_COMMAND);
    }

    private boolean isSearchBotCommand(Update update) {
        return isBotCommandStartsWith(update,
                SEARCH_BOT_COMMAND);
    }

    private boolean isManBotCommand(Update update) {
        return isBotCommandStartsWith(update,
                MAN_BOT_COMMAND);
    }

    private boolean isBotCommandStartsWith(Update update, String searchBotCommand) {
        return update.getMessage()
                .getEntities()
                .get(0)
                .getText()
                .startsWith(searchBotCommand);
    }

    private boolean isBotCommandMessage(Update update) {
        return update.hasMessage()
                && update.getMessage()
                .hasText()
                && update.getMessage()
                .hasEntities()
                && update.getMessage()
                .getEntities()
                .get(0)
                .getType()
                .equals(BOT_COMMAND_TYPE);
    }

    @Override
    public String getBotUsername() {
        return credentials.getBotUsername();
    }
}
