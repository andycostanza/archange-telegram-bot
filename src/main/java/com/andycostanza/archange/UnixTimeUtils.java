package com.andycostanza.archange;

import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

public final class UnixTimeUtils {
    public static String toDateTime(Long unixTime) {
        var formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        return Instant.ofEpochMilli(unixTime * 1000L)
                .atZone(ZoneId.of("UTC"))
                .toLocalDateTime()
                .format(formatter) + " (UTC)";
    }

    public static String toDate(Long unixTime) {
        var formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        return Instant.ofEpochMilli(unixTime * 1000L)
                .atZone(ZoneId.of("UTC"))
                .toLocalDateTime()
                .format(formatter);
    }
}
