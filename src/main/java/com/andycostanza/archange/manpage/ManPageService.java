package com.andycostanza.archange.manpage;

import org.telegram.telegrambots.meta.api.methods.send.SendMessage;

import java.io.Serializable;
import java.util.List;

public interface ManPageService extends Serializable {
    List<SendMessage> search(Long chatId, String keyword);
}
