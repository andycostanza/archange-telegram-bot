package com.andycostanza.archange.manpage;

import com.andycostanza.archange.CustomFeignConfiguration;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(name="man-service", url = "https://man.archlinux.org/man", configuration = CustomFeignConfiguration.class)
public interface ManPageFeignClient {
    @RequestMapping(method = RequestMethod.GET, value = "/{keyword}.fr.txt")
    ResponseEntity<String> search(@PathVariable("keyword") String keyword);
}
