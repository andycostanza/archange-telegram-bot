package com.andycostanza.archange.manpage;

import feign.FeignException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;

import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor
@Service
@Slf4j
public class ManPageServiceImpl implements ManPageService {
    private final ManPageFeignClient manPageFeignClient;

    @Override
    public List<SendMessage> search(Long chatId, String keyword) {
        List<SendMessage> messages = new ArrayList<>();
        try {
            var responseEntity = manPageFeignClient.search(keyword);

            if (responseEntity.getStatusCode()
                    .equals(HttpStatus.NOT_FOUND)) {
                messages.add(SendMessage.builder()
                        .chatId(Long.toString(chatId))
                        .text("Nothing found !")
                        .build());
            } else if (responseEntity.getStatusCode()
                    .equals(HttpStatus.OK)) {
                var manPage = responseEntity.getBody();
                if (StringUtils.isNotBlank(manPage)) {
                    messages.add(SendMessage.builder()
                            .chatId(Long.toString(chatId))
                            .text(StringUtils.abbreviate(manPage,
                                    4096))
                            .build());
                    if (manPage.length() > 4096) {
                        messages.add(SendMessage.builder()
                                .chatId(Long.toString(chatId))
                                .text("See full man page on https://man.archlinux.org/man/" + keyword + ".fr.html")
                                .build());

                    }
                }
            }
        } catch (FeignException e) {
            log.error(e.getMessage(),e);
            messages.add(SendMessage.builder()
                    .chatId(Long.toString(chatId))
                    .text("Nothing found !")
                    .build());
        }
        return messages;
    }
}
