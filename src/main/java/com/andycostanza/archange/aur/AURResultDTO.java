package com.andycostanza.archange.aur;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
public class AURResultDTO implements Serializable {
    @JsonProperty("ID")
    private Long id;
    @JsonProperty("Name")
    private String name;
    @JsonProperty("PackageBaseID")
    private Long packageBaseID;
    @JsonProperty("PackageBase")
    private String packageBase;
    @JsonProperty("Version")
    private String version;
    @JsonProperty("Description")
    private String description;
    @JsonProperty("URL")
    private String uRL;
    @JsonProperty("NumVotes")
    private Long numVotes;
    @JsonProperty("Popularity")
    private Double popularity;
    @JsonProperty("OutOfDate")
    private Long outOfDate;
    @JsonProperty("Maintainer")
    private String maintainer;
    @JsonProperty("FirstSubmitted")
    private Long firstSubmitted;
    @JsonProperty("LastModified")
    private Long lastModified;
    @JsonProperty("URLPath")
    private String uRLPath;
    @JsonProperty("Depends")
    private List<String> depends;
    @JsonProperty("MakeDepends")
    private List<String> makeDepends;
    @JsonProperty("OptDepends")
    private List<String> optDepends;
    @JsonProperty("CheckDepends")
    private List<String> checkDepends;
    @JsonProperty("Conflicts")
    private List<String> conflicts;
    @JsonProperty("Provides")
    private List<String> provides;
    @JsonProperty("Replaces")
    private List<String> replaces;
    @JsonProperty("Groups")
    private List<String> groups;
    @JsonProperty("License")
    private List<String> licences;
    @JsonProperty("Keywords")
    private List<String> keywords;


}
