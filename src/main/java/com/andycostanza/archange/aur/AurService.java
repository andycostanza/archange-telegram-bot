package com.andycostanza.archange.aur;

import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.inlinequery.result.InlineQueryResult;

import java.io.Serializable;
import java.util.List;

public interface AurService extends Serializable {
    List<SendMessage> search(Long chatId, String packageName);

    List<InlineQueryResult> inlineQuerySearch(String query);

    List<SendMessage> info(Long chatId, String packageName);
}
