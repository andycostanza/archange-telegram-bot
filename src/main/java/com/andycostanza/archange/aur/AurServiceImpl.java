package com.andycostanza.archange.aur;

import com.andycostanza.archange.UnixTimeUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.telegram.telegrambots.meta.api.methods.ParseMode;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.inlinequery.inputmessagecontent.InputTextMessageContent;
import org.telegram.telegrambots.meta.api.objects.inlinequery.result.InlineQueryResult;
import org.telegram.telegrambots.meta.api.objects.inlinequery.result.InlineQueryResultArticle;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Service
@RequiredArgsConstructor
public class AurServiceImpl implements AurService {
    public static final String AUR_API_VERSION = "5";
    public static final String AUR_TYPE_SEARCH = "search";
    public static final String AUR_TYPE_INFO = "info";
    private final AURFeignClient feignClient;

    @Override
    public List<SendMessage> info(Long chatId, String packageName) {
        var response = feignClient.search(AUR_API_VERSION,
                                          AUR_TYPE_INFO,
                                          packageName);
        List<SendMessage> messages = new ArrayList<>();
        if (response.isPresent() && !CollectionUtils.isEmpty(response.get()
                                                                     .getResults())) {
            messages = response.get()
                    .getResults()
                    .stream()
                    .map(result -> {
                        var sendMessage = SendMessage.builder()
                                .chatId(Long.toString(chatId))
                                .text(createInfoMessage(result))
                                .build();
                        sendMessage.setParseMode(ParseMode.HTML);
                        return sendMessage;
                    })
                    .collect(Collectors.toList());

        }
        return messages;
    }

    @Override
    public List<SendMessage> search(Long chatId, String packageName) {
        var response = feignClient.search(AUR_API_VERSION,
                                          AUR_TYPE_SEARCH,
                                          packageName);
        List<SendMessage> messages = new ArrayList<>();
        if (response.isPresent()) {
            if (!CollectionUtils.isEmpty(response.get()
                                                 .getResults())) {
                /*var partition =
                        ListUtils.partition(response.get()
                                                    .getResults(),
                                            20);*/
                var responseList = response.get()
                        .getResults();
                var partition = IntStream.range(0, responseList.size())
                        .filter(i -> i % 20 == 0)
                        .mapToObj(i -> responseList.subList(i, Math.min(i + 20, responseList.size())))
                        .toList();
                for (var list :
                        partition) {
                    var message = list.stream()
                            .map(result -> "- " + createExcerptMessage(result))
                            .collect(Collectors.joining("\n"));
                    var sendMessage = SendMessage.builder()
                            .chatId(Long.toString(chatId))
                            .text(message)
                            .build();
                    sendMessage.setParseMode(ParseMode.HTML);
                    messages.add(sendMessage);
                }
            } else if (response.get()
                    .getType()
                    .equals("error")) {
                messages.add(SendMessage.builder()
                                     .chatId(Long.toString(chatId))
                                     .text(response.get()
                                                   .getError())
                                     .build());
            }
        } else {
            messages.add(SendMessage.builder()
                                 .chatId(Long.toString(chatId))
                                 .text("Nothing found in AUR!")
                                 .build());
        }
        return messages;
    }

    @Override
    public List<InlineQueryResult> inlineQuerySearch(String query) {
        var response = feignClient.search(AUR_API_VERSION,
                                          AUR_TYPE_INFO,
                                          query);
        List<InlineQueryResult> results = new ArrayList<>();
        if (response.isPresent() && !CollectionUtils.isEmpty(response.get()
                                                                       .getResults())) {
            results = response.get()
                    .getResults()
                    .stream()
                    .map(aurResultDTO -> createInlineQueryResultArticle(createTitle(aurResultDTO),
                                                                        createDescription(aurResultDTO),
                                                                        createInfoMessage(aurResultDTO)))
                    .collect(Collectors.toList());

        }
        return results;
    }


    private String createInfoMessage(AURResultDTO result) {
        var message = new StringBuilder();
        message
                .append("Repository : <code>")
                .append("aur")
                .append("</code>\n")
                .append("Package name : <code>")
                .append(result.getName())
                .append("</code>\n")
                .append("Keywords : <code>")
                .append(result.getKeywords())
                .append("</code>\n")
                .append("Version : <code>")
                .append(result.getVersion())
                .append("</code>\n")
                .append("Description : <code>")
                .append(result.getDescription())
                .append("</code>\n")
                .append("URL : ")
                .append(result.getURL())
                .append("\n")
                .append("Groups : <code>")
                .append(!CollectionUtils.isEmpty(result.getGroups()) ? result.getGroups() : "--")
                .append("</code>\n")
                .append("Licenses : <code>")
                .append(!CollectionUtils.isEmpty(result.getLicences()) ? result.getLicences() : "--")
                .append("</code>\n")
                .append("Provides : <code>")
                .append(!CollectionUtils.isEmpty(result.getProvides()) ? result.getProvides() : "--")
                .append("</code>\n")
                .append("Depends : <code>")
                .append(!CollectionUtils.isEmpty(result.getDepends()) ? result.getDepends() : "--")
                .append("</code>\n")
                .append("Opt. depends : <code>")
                .append(!CollectionUtils.isEmpty(result.getOptDepends()) ? result.getOptDepends() : "--")
                .append("</code>\n")
                .append("Conflicts : <code>")
                .append(!CollectionUtils.isEmpty(result.getConflicts()) ? result.getConflicts() : "--")
                .append("</code>\n")
                .append("Maintainer : <code>")
                .append(result.getMaintainer())
                .append("</code>\n")
                .append("Votes : <code>+")
                .append(result.getNumVotes())
                .append("</code>\n")
                .append("Popularity : <code>")
                .append(result.getPopularity())
                .append("</code>\n")
                .append("First submitted : <code>")
                .append(UnixTimeUtils.toDateTime(result.getFirstSubmitted()))
                .append("</code>\n")
                .append("Last modified : <code>")
                .append(UnixTimeUtils.toDateTime(result.getLastModified()))
                .append("</code>\n")
                .append("Flagged : <code>")
                .append(result.getOutOfDate() != null ? "Yes" : "No")
                .append("</code>\n");

        return message.toString();
    }

    private String createDescription(AURResultDTO result) {
        return result.getDescription();
    }

    private String createExcerptMessage(AURResultDTO result) {
        var message = new StringBuilder();
        message.append("<strong>aur/")
                .append(result.getName())
                .append("</strong> <code>")
                .append(result.getVersion())
                .append("</code> (+")
                .append(result.getNumVotes())
                .append(" ")
                .append(result.getPopularity())
                .append(") ");
        if (result.getOutOfDate() != null) {
            message.append("<strong>(Flagged")
                    .append(" : ")
                    .append(UnixTimeUtils.toDate(result.getOutOfDate()))
                    .append(")</strong>");
        }
        message.append("\n\t<em>")
                .append(result.getDescription())
                .append("</em>");
        return message.toString();
    }

    private InlineQueryResultArticle createInlineQueryResultArticle(String title, String description, String messageText) {
        var resultArticle = new InlineQueryResultArticle();
        resultArticle.setId(UUID.randomUUID()
                                    .toString());
        resultArticle.setTitle(title);
        resultArticle.setDescription(description);
        var inputMessageContent = new InputTextMessageContent();
        inputMessageContent.setMessageText(messageText);
        inputMessageContent.setParseMode(ParseMode.HTML);
        resultArticle.setInputMessageContent(inputMessageContent);
        return resultArticle;
    }

    private String createTitle(AURResultDTO result) {
        var message = new StringBuilder();
        message.append("aur/")
                .append(result.getName())
                .append(" ")
                .append(result.getVersion());
        if (result.getOutOfDate() != null) {
            message.append(" (Flagged : ")
                    .append(UnixTimeUtils.toDate(result.getOutOfDate()))
                    .append(")");
        }
        return message.toString();
    }
}
