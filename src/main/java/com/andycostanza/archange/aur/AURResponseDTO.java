package com.andycostanza.archange.aur;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
public class AURResponseDTO implements Serializable {
    private Integer version;
    private String type;
    private Integer resultcount;
    @JsonProperty("results")
    private List<AURResultDTO> results;
    private String error;
}
