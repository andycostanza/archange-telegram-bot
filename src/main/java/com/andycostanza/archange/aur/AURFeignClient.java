package com.andycostanza.archange.aur;

import com.andycostanza.archange.CustomFeignConfiguration;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.Serializable;
import java.util.Optional;

@FeignClient(name="aur-service", url = "https://aur.archlinux.org/rpc", configuration = CustomFeignConfiguration.class)
public interface AURFeignClient extends Serializable {
    @RequestMapping(method = RequestMethod.GET, value = "/")
    Optional<AURResponseDTO> search(@RequestParam("v") String v, @RequestParam("type") String type, @RequestParam("arg") String arg);
}
