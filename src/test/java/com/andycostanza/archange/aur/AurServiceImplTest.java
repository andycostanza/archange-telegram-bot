package com.andycostanza.archange.aur;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;
@ExtendWith(MockitoExtension.class)
class AurServiceImplTest {
    AurService service;
    @Mock
    AURFeignClient feignClient;

    @BeforeEach
    void setUp() {
        service = new AurServiceImpl(feignClient);
    }

    @Test
    @DisplayName("info should return empty list if feign client return an Optional.empty")
    void info() {
        Mockito.when(feignClient.search(Mockito.anyString(),
                                        Mockito.anyString(),
                                        Mockito.anyString())).thenReturn(Optional.empty());
        List<SendMessage> result = service.info(1L,
                                                     "packageName");
        Assertions.assertEquals(Collections.emptyList(), result);

    }

    @Test
    void search() {
        long l = 1439142013L*1000;
        Date date = new Date(l);
        System.out.println(date);
    }

    @Test
    void inlineQuerySearch() {
    }
}