# Archange telegram bot
Archange is a Telegram bot to find package in AUR and official repositories

## Telegram Documentation
https://core.telegram.org/api

https://github.com/rubenlagus/TelegramBots

## Archlinux API Documentation
https://wiki.archlinux.org/index.php/Official_repositories_web_interface

Sample request to search pacman in official repositories :

`https://www.archlinux.org/packages/search/json/?q=pacman`

https://wiki.archlinux.org/index.php/Aurweb_RPC_interface

Sample request to search spotify in AUR :

`https://aur.archlinux.org/rpc/?v=5&type=search&arg=spotify`
